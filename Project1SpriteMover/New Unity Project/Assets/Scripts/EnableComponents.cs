﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableComponents : MonoBehaviour
{
    private PlayerMovement myPlayerMovement;
    // Start is called before the first frame update
    void Start()
    {
        myPlayerMovement = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        //Diasble movement components function
        if (Input.GetKeyUp(KeyCode.P))
        {
            myPlayerMovement.enabled = !myPlayerMovement.enabled;
        }
    }
}
