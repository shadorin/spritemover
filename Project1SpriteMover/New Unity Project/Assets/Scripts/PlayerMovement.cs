﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Transform tf; //variable for our transform component

    public float speed=.01f;// variable for desginers to control the speed in unity editor

    private Vector3 startPosition;// variable to reset player position
    
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the player holds shift the Sprite only moves one unit each time the button is pressed
        if (Input.GetKey(KeyCode.LeftShift))
        {

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                tf.position = tf.position + Vector3.left * 1f;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                tf.position = tf.position + Vector3.right * 1f;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                tf.position = tf.position + Vector3.up * 1f;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                tf.position = tf.position + Vector3.down * 1f;
            }
        }
        //If the player is not holding the Shift button the player will move at a constant speed set by the desginer
        else
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                tf.position = tf.position + Vector3.left * speed;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                tf.position = tf.position + Vector3.right * speed;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                tf.position = tf.position + Vector3.up * speed;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                tf.position = tf.position + Vector3.down * speed;
            }
        }
        //Player reset position function 
        if (Input.GetKey(KeyCode.Space))
        {
            tf.position=   startPosition; 
        }
   
    }
}
