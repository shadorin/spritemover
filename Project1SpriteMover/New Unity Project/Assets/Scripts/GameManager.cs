﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject playerShip;//variable for controlling the PlayerShip GameObject with the GameManager GameObject 

    public int playerShipHealth=100;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        //Function for disbaling/enabling the PlayerShip GameObject
        if (Input.GetKeyDown(KeyCode.Q))
        {
            playerShip.SetActive(!playerShip.activeInHierarchy);
        }
        //Function for exiting the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (playerShipHealth <= 0)
        {
            Application.Quit();
        }
    }
}
